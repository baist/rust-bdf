use std::env;

fn print_glyph(font: &bdf::Font, glyph: &bdf::Glyph)
{
    for _ in 0..(font.bounds().height - glyph.bounds().height) as i32 - glyph.bounds().y
        + font.bounds().y
    {
        for _ in 0..font.bounds().width {
            print!("a");
        }

        print!("\n");
    }

    for y in 0..glyph.height() {
        for _ in 0..glyph.bounds().x {
            print!("b");
        }

        for x in 0..glyph.width() {
            if glyph.get(x, y) {
                print!("1,");
            } else {
                print!("0,");
            }
        }

        for _ in 0..(font.bounds().width - glyph.bounds().width) as i32 - glyph.bounds().x {
            print!("c");
        }

        print!("\n");
    }

    for _ in 0..font.bounds().y.abs() + glyph.bounds().y {
        for _ in 0..font.bounds().width {
            print!("d");
        }

        print!("\n");
    }
}

fn main()
{
    let path = env::args().nth(1).expect("missing font file");
    let font = bdf::open(path).unwrap();

    println!("const DATA: [u8; 27360] = [");
    for c in 'A'..='~' {
        if let Some(glyph) = font.glyphs().get(&c) {
            println!("// {}", glyph.name());
            print_glyph(&font, &glyph);
            println!(" ");
        }
    }
    println!("];");
}
